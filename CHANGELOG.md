# Changelog

## [0.1.3] - 2024-04-30
 - Correctly handle categorized renderers based on a numeric field (#55)
 - Fixing layer order for graduated maps

## [0.1.2] - 2019-06-20
 - Fixes fill layer label placed incorrectly (#50)
 - Multiple symbol layers now display in correct order (#52)
 - Fill layer no brush property now controls fill color (#53)
 - Circle layer will not generate two GL layers (#54)

## [0.1.1] - 2019-05-20
 - Fixes bugs with color not using alpha channel (#45)
 - Created checks for unsupported symbol layer and simplified layer class
   selection logic (#46)
 - Removed incorrect visibility logic from layer converter (#51)

## [0.1.0] - 2019-05-02
 - Initial release
