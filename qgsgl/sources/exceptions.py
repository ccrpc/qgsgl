

class LayerNotSetError(Exception):
    pass


class OGRError(Exception):
    pass
