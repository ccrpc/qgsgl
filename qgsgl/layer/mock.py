class MockLayer:
    def __init__(self, qgis_symbol_layer=None, **kwargs):
        pass

    @classmethod
    def supports_symbol_layer(cls, symbol_layer):
        return True

    def get_type(self):
        return 'mock'

    def get_layout_properties(self):
        return {}

    def get_paint_properties(self):
        return {}


class NullMockLayer(MockLayer):
    @classmethod
    def supports_symbol_layer(cls, symbol_layer):
        return False
