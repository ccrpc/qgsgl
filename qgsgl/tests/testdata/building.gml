<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ building.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>-88.2578033208847</gml:X><gml:Y>40.11138096591307</gml:Y></gml:coord>
      <gml:coord><gml:X>-88.21433275938034</gml:X><gml:Y>40.12207989210422</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                               
  <gml:featureMember>
    <ogr:building fid="1">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>-88.2578033208847,40.121911708819 -88.2574546337128,40.121911708819 -88.2574546337128,40.1220798921042 -88.2578033208847,40.1220798921042 -88.2578033208847,40.121911708819</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:name>Building One</ogr:name>
      <ogr:height>5</ogr:height>
      <ogr:city>Champaign</ogr:city>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="2">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>-88.2144749164581,40.1113809659131 -88.2143327593803,40.1113809659131 -88.2143327593803,40.1114384032929 -88.2144749164581,40.1114384032929 -88.2144749164581,40.1113809659131</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:name>School Building</ogr:name>
      <ogr:height>4</ogr:height>
      <ogr:city>Urbana</ogr:city>
    </ogr:building>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:building fid="3">
      <ogr:geometryProperty><gml:Polygon srsName="EPSG:4326"><gml:outerBoundaryIs><gml:LinearRing><gml:coordinates>-88.2176667451859,40.1122712398493 -88.217516541481,40.1122712398493 -88.217516541481,40.1123327790917 -88.2176667451859,40.1123327790917 -88.2176667451859,40.1122712398493</gml:coordinates></gml:LinearRing></gml:outerBoundaryIs></gml:Polygon></ogr:geometryProperty>
      <ogr:name>Store Building</ogr:name>
      <ogr:height xsi:nil="true"/>
      <ogr:city xsi:nil="true"/>
    </ogr:building>
  </gml:featureMember>
</ogr:FeatureCollection>
