<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ street.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>1013700.863326282</gml:X><gml:Y>1239712.179563166</gml:Y></gml:coord>
      <gml:coord><gml:X>1018092.4036923</gml:X><gml:Y>1243113.391048123</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                  
  <gml:featureMember>
    <ogr:street fid="1">
      <ogr:geometryProperty><gml:LineString srsName="EPSG:3435"><gml:coordinates>1013700.86332628,1243113.39104812 1018084.28624246,1243113.39104812</gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:name>Main St</ogr:name>
      <ogr:us_route xsi:nil="true"/>
      <ogr:state_route>99</ogr:state_route>
    </ogr:street>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:street fid="2">
      <ogr:geometryProperty><gml:LineString srsName="EPSG:3435"><gml:coordinates>1013700.86332628,1243113.39104812 1013700.86332628,1240856.73999128 1013773.92037488,1240239.81380308 1013757.6854752,1239712.17956317</gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:name>First Ave</ogr:name>
      <ogr:us_route>4</ogr:us_route>
      <ogr:state_route xsi:nil="true"/>
    </ogr:street>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:street fid="3">
      <ogr:geometryProperty><gml:LineString srsName="EPSG:3435"><gml:coordinates>1018084.28624246,1243113.39104812 1018092.4036923,1241554.84067793</gml:coordinates></gml:LineString></ogr:geometryProperty>
      <ogr:name>Sixth Ave</ogr:name>
      <ogr:us_route xsi:nil="true"/>
      <ogr:state_route xsi:nil="true"/>
    </ogr:street>
  </gml:featureMember>
</ogr:FeatureCollection>
