<?xml version="1.0" encoding="utf-8" ?>
<ogr:FeatureCollection
     xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://ogr.maptools.org/ station.xsd"
     xmlns:ogr="http://ogr.maptools.org/"
     xmlns:gml="http://www.opengis.net/gml">
  <gml:boundedBy>
    <gml:Box>
      <gml:coord><gml:X>1009837.447119104</gml:X><gml:Y>1240506.228338044</gml:Y></gml:coord>
      <gml:coord><gml:X>1017248.400080397</gml:X><gml:Y>1248051.925898634</gml:Y></gml:coord>
    </gml:Box>
  </gml:boundedBy>
                                                                                                
  <gml:featureMember>
    <ogr:station fid="1">
      <ogr:geometryProperty><gml:Point srsName="EPSG:3435"><gml:coordinates>1017248.4000804,1248051.92589863</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:name>Station One</ogr:name>
      <ogr:number>1</ogr:number>
    </ogr:station>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:station fid="2">
      <ogr:geometryProperty><gml:Point srsName="EPSG:3435"><gml:coordinates>1015564.09258919,1240506.22833804</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:name>Station Five</ogr:name>
      <ogr:number>5</ogr:number>
    </ogr:station>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:station fid="3">
      <ogr:geometryProperty><gml:Point srsName="EPSG:3435"><gml:coordinates>1009837.4471191,1243605.35412186</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:name>Station Twelve</ogr:name>
      <ogr:number>12</ogr:number>
    </ogr:station>
  </gml:featureMember>
  <gml:featureMember>
    <ogr:station fid="4">
      <ogr:geometryProperty><gml:Point srsName="EPSG:3435"><gml:coordinates>1015025.11419201,1241382.06823347</gml:coordinates></gml:Point></ogr:geometryProperty>
      <ogr:name>Station Four</ogr:name>
      <ogr:number>4</ogr:number>
    </ogr:station>
  </gml:featureMember>
</ogr:FeatureCollection>
